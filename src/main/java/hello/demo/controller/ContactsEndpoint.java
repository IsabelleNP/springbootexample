package hello.demo.controller;

import hello.demo.services.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class ContactsEndpoint {

    @Autowired
    private ContactService contactService;

    @GetMapping("/contact/{id}")
    public ResponseEntity<Contact> getContact(@PathVariable Integer id) {
        Optional<Contact> contact = contactService.getContact(id);

        if (contact.isPresent()){
            return ResponseEntity.ok(contact.get());
        }

        return ResponseEntity.notFound().build();
    }

    @GetMapping("/contact")
    public ResponseEntity<Contact> getContactByName(@RequestParam(value = "firstName") String firstName) {
        return ResponseEntity.accepted().build();
    }

}
