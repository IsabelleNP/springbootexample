package hello.demo.services;

import hello.demo.controller.Contact;
import hello.demo.dao.ContactDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ContactService {

    @Autowired
    private ContactDAO contactDAO;

    public Optional<Contact> getContact(Integer id) {
        Optional<ContactEntity> contactOpt = contactDAO.findById(id);

        if (contactOpt.isPresent()) {
            ContactEntity entidade = contactOpt.get();
            Contact contact = new Contact();
            contact.setLastName(entidade.getLastName());
            contact.setFirstName(entidade.getFirstName());
            contact.setId(entidade.getId());

            return Optional.of(contact);
        }

        return Optional.empty();
    }
}
