package hello.demo.dao;

import hello.demo.services.ContactEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactDAO extends JpaRepository<ContactEntity, Integer> {

}
