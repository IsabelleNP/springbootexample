package hello.demo;

import hello.demo.dao.ContactDAO;
import hello.demo.services.ContactEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
//@SpringBootTest(value = { "SpringBootTest.WebEnvironment.MOCK" }, classes = DemoApplication.class)
@SpringBootTest
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = { DemoApplication.class })
@AutoConfigureMockMvc
public class DemoApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ContactDAO contactDAO;

	@Test
	public void returnsContactData() throws Exception {
		ContactEntity entity = new ContactEntity();
		entity.setId(1);
		entity.setFirstName("Matheus");
		entity.setLastName("Teste");
		contactDAO.save(entity);

		this.mockMvc.perform(get("/contact/1"))
				.andDo(print())
				.andExpect(jsonPath("$.id").value("1"))
				.andExpect(jsonPath("$.firstName").value("Matheus"))
				.andExpect(jsonPath("$.lastName").value("Teste"));
	}

	@Test
	public void idNotExistentReturnsNotFound() throws Exception {
		this.mockMvc.perform(get("/contact/1"))
			.andExpect(status().isNotFound());
	}

	@Test
	public void searchContactByName() throws Exception{
		ContactEntity entity = new ContactEntity();
		entity.setId(1);
		entity.setFirstName("Fulano");
		entity.setLastName("Teste");
		contactDAO.save(entity);

		this.mockMvc.perform(get("/contact?firstName=Fulano")).andExpect(status().isOk());
	}

}
